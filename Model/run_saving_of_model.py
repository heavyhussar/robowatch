import save_and_load_model as sl

# short script for loading the AI and saving them to a file

model = sl.load_model()
sl.save_model_to_file(model)

model = sl.load_weights(model)
sl.save_model_weights(model)
print("saved model successfully!")
