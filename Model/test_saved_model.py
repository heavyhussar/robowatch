import save_and_load_model as sl

# Test if the model is able to load from a file
model = sl.load_model()
model = sl.load_weights_from_file(model)
print("Loaded successfully")
