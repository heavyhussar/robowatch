import create_embeddings
import numpy as np
import cv2
from resource_files import helpers as hv
import re


# sends the captured face to the AI and compare the similarity of the captured face with the faces loaded into the
# program, and if there is a match (where the similarity is 40 percent or greater),
# the function returns the name of the face
def recognize_face(face_image, input_embeddings, model):
    embedding = create_embeddings.image_to_embedding(face_image, model)

    minimum_distance = 200
    name = None

    # Loop over  names and encodings.
    for (input_name, input_embedding) in input_embeddings.items():

        euclidean_distance = np.linalg.norm(embedding - input_embedding)

        print('Euclidean distance from %s is %s' % (input_name, euclidean_distance))

        if euclidean_distance < minimum_distance:
            minimum_distance = euclidean_distance
            name = input_name

    if minimum_distance < 0.68:
        return str(name)
    else:
        return None


# takes a live feed from a webcam, captures a face, and sends the face captured from the live feed to the AI for
# comparison
def recognize_faces_in_cam(input_embeddings, model):
    cv2.namedWindow("Face Recognizer")
    vc = cv2.VideoCapture(0)

    font = cv2.FONT_HERSHEY_SIMPLEX

    # This program uses openCV, a python library for processing images and video, to extract the face and
    # send it to the AI
    face_cascade = cv2.CascadeClassifier(hv.join_three_path_elements(hv._move_one_directory_up,
                                                                     hv._resource_files, hv._haar_cascade))

    while vc.isOpened():
        # read the data from the camera and get the feed's pixel dimensions
        # (example: size of an HD image is 480 x 740 pixels)
        _, frame = vc.read()
        img = frame
        height, width, channels = frame.shape

        # convert image to greyscale and then run face detection
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        faces = face_cascade.detectMultiScale(gray, 1.3, 5)

        # Loop through all the faces detected
        for (x, y, w, h) in faces:
            x1 = x
            y1 = y
            x2 = x + w
            y2 = y + h

            # extract face from feed
            face_image = frame[max(0, y1):min(height, y2), max(0, x1):min(width, x2)]

            # perform facial identification with extracted face
            identity = recognize_face(face_image, input_embeddings, model)

            # draws a rectangle around selected face
            img = cv2.rectangle(frame, (x1, y1), (x2, y2), (255, 255, 255), 2)

            # display the name of the person if they are found in the database, and "unknown" otherwise
            if identity is not None:
                identity = re.sub("\W+", "", identity)
                identity = re.sub("\d+", "", identity)
                cv2.putText(img, str(identity), (x1 + 5, y1 - 5), font, 1, (255, 255, 255), 2)
            else:
                cv2.putText(img, "unknown", (x1 + 5, y1 - 5), font, 1, (255, 255, 255), 2)

        # display the feed with the highlight and person's name (or unknown if the AI does not recognize them)
        key = cv2.waitKey(100)
        cv2.imshow("Face Recognizer", img)

        if key == 27:  # exit on ESC
            break

    # release the webcam feed and close all windows after execution is complete
    vc.release()
    cv2.destroyAllWindows()
