import save_and_load_model as sl
import create_embeddings
import face_detection
import cv2
from resource_files import helpers as hv


# assembles the AI, takes the images saved to the folder, gets a live camera feed that is sent to the AI, and then
# displays the output on a frame of the live feed
def run_face_detection():

    # check to see if images directory is empty and give the user a warning if so
    is_empty = hv.see_if_directory_is_empty(hv.join_two_path_elements(hv._move_one_directory_up, hv._image_files))

    if is_empty is False:
        print("Warning! The images directory is empty. "
              "The system will not be able to identify any of the faces that appear"
              "in the camera. To fix this, run the extract_face_to_image script to save facial images.")

    # check to see if there is or is not a video feed and stop the program if so
    vc = cv2.VideoCapture(0)

    if vc is None or not vc.isOpened():
        print("A webcam is not detected or it is already in use. Exiting Program.")
        quit()

    # these two functions below assemble the AI and load its memory in
    face_detection_model = sl.load_model()
    face_detection_model = sl.load_weights(face_detection_model)

    # gets files from the images directory and turns them into data that the AI can process
    input_embeddings = create_embeddings.create_input_image_embeddings(face_detection_model)

    # runs the actual face detection using the data returned from the create_embeddings function
    # the output is displayed as a box around the face of the person with the name above
    face_detection.recognize_faces_in_cam(input_embeddings, face_detection_model)


# runs the above function when this python file is run
if __name__ == "__main__":
    run_face_detection()
