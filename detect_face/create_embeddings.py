import cv2
import numpy as np
import glob
import os
from resource_files import helpers as hv


# turns an individual image into data the AI can read
def image_to_embedding(image, model):
    image = cv2.resize(image, (96, 96))
    img = image[...,::-1]
    img = np.around(np.transpose(img, (0,1,2))/255.0, decimals=12)
    x_train = np.array([img])
    embedding = model.predict_on_batch(x_train)
    return embedding


# runs the image_to_embedding function on all the images in the images directory
def create_input_image_embeddings(model):
    input_embeddings = {}

    for file in glob.glob(hv.join_two_path_elements(hv._move_one_directory_up, hv._image_files) + "/*/*"):
        person_name = os.path.splitext(os.path.basename(file))[0]
        image_file = cv2.imread(file, 1)
        input_embeddings[person_name] = image_to_embedding(image_file, model)

    return input_embeddings
