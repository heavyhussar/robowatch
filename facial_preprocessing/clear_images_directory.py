from resource_files import helpers as hv

continue_on = True

# clear out the images directory upon running. This program lets the user choose between clearing the entire images
# directory or a subdirectory. You can also exit the script if you so choose.

def clear_images_directory():
    while continue_on is True:
        clear_full_directory_or_subdirectory = \
            input("Do you want to clear the entire images directory or a subdirectory? "
                  "Y = yes, C = Clear subdirectory, Q = quit program")

        clear_full_directory_or_subdirectory = str.lower(clear_full_directory_or_subdirectory)

        # clears the entire images directory, including all folders, subdirectories, and files
        if clear_full_directory_or_subdirectory == "y":
            hv.clear_directory(hv.join_two_path_elements(hv._move_one_directory_up, hv._image_files))
            print("Done clearing images directory!")
            continue_on = False

        # clears user-specified subdirectory if it exists
        elif clear_full_directory_or_subdirectory == "c":

            directory_to_clear = input("Enter the directory you want to clear:")

            check_if_dir_exists = hv.see_if_directory_exists(directory_to_clear, hv.join_two_path_elements(hv._move_one_directory_up, hv._image_files))

            if check_if_dir_exists is True:
                print("clearing directory")
                hv.clear_directory(hv.join_three_path_elements(hv._move_one_directory_up, hv._image_files, directory_to_clear))

            else:
                print("directory does not exist. Exiting Program.")

            print("exiting program")
            continue_on = False
            quit()

        # quit the program if the user doesn't want to continue
        elif clear_full_directory_or_subdirectory == "q":
            print("exiting program")
            continue_on = False
            quit()

        else:
            print("Please provide a response from the options listed above.")

# run the above function when the script is started by the user
if __name__ == "__main__":
    clear_images_directory()
