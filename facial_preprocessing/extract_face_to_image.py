import cv2
import numpy as np
from resource_files import helpers as hv

# script that extracts 10 faces from a webcam feed and saves them to the images folder using a name specified by
# the user. The AI will use the filename as the name that will display on the screen. Each time this is run, the files
# are saved to a subdirectory for organization purposes

# load the face detection system
face_extractor = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")

# Sees if there is a webcam available before proceeding with the program
capture = cv2.VideoCapture(0)

if capture is None or not capture.isOpened():
    print("A webcam is not detected or it is already in use. Exiting Program")
    quit()

images_already_saved = 0
faces_saved = 0

# get the name of the person that is being saved to the images directory
name_of_person = input("Enter the name of the person you are saving:")

print("the name of the person you entered is {}".format(name_of_person))

path_to_save = hv.join_three_path_elements(hv._move_one_directory_up, hv._image_files, name_of_person)

# count all the files saved in that directory if there are any
files = hv.get_all_files(path_to_save)
for file in files:
    images_already_saved += 1

# create the directory
if hv.see_if_directory_exists(path_to_save) is True:
    print("Directory already exists. The images will be saved to that directory")
else:
    hv.create_directory(path_to_save)

while True:
    # run face detection and extraction
    ret, img = capture.read()
    detected_face = face_extractor.detectMultiScale(img, 1.3, 5)

    for (x, y, w, h) in detected_face:
        print(faces_saved)
        extracted_image = img[y:(y + h), x:(x + w)]

        # Save extracted face to images directory
        hv.save_image_to_file(hv.join_two_path_elements(path_to_save,
                                                        (name_of_person + str(images_already_saved +
                                                                              faces_saved)) + ".jpg"),
                              extracted_image)
        print("Saved image to the images folder.")

        faces_saved += 1

    # stop the program if the number of faces saved is greater than or equal to 10
    if faces_saved >= 10:
        break

    # stop the program if the user hits the escape key
    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break

# release the webcam and close all the windows that openCV might have open after the program is finished executing.
print("Done with saving images.")
capture.release()
cv2.destroyAllWindows()
