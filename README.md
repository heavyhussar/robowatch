Working with the RoboWatch Repository
---

##Repository Link:

Link: https://bitbucket.org/heavyhussar/robowatch/src/master/

## Explanation of Project

Robowatch is a small example of how AI is able to recognize and identify faces. The program works by extracting a face
out of a live webcam feed and comparing it to a list of images. If the face resembles an image from the list, it will
display the name of the person. If it does not recognize the person, it will display "unknown". The project can also
save 10 images of a specified face to the list so that way new identities can be added. You can also add them directly
if you so choose. 

---

## Items Needed
1. Webcam
2. Python Development Environment
3. Installed Libraries (Read the instructions for how to install these libraries below.)

___

## How to Install Python Libraries
1. Install Python 3.6 or above. Link to download: https://www.python.org/downloads/
2. After Python is installed, open a command prompt. Enter "python" to check if it is installed and check what version
you have. 
3. Now open either a python console or command prompt window where you have downloaded or opened the project.
4. Use "pip install -r requirements.txt" to install all of the required libraries. 

___

## Extract Face to Image

1. Run the extract_face_to_image.py script with your python IDE or console.
2. The program will ask you to enter a name of the face. This will be what the AI displays on the output when the program
recognizes this face.
3. The program will then save 10 images of that person's face in a subdirectory labeled with the name of that person.
Note: If the directory already exists, it will save those 10 images in the same file.

---

## Run Face Detection

1. Run the run_face_detection.py script. This will start the AI and load all of the images saved in the images folder.
Note: This AI will take a little while to load because of it's size and complexity. 
2. After the AI is done loading, a live feed will appear from the webcam. White rectangles will be drawn around all
detected faces, with the person's name displayed above. If the program doesn't recognize their face, it will display
"unknown" at the top of the page.
3. Press the escape key to close the program and stop the thread from running.

---

## Clear the images folder

1. Start the clear_images_directory.py script.
2. When prompted, enter "Y" to clear your entire directory, enter "C" to clear a subdirectory of your choice, or press
"Q" to quit the program. Be sure to enter the subdirectory of your choice.
3. The program will exit once the directory or subdirectory is clear, or you have quitted the program.

---

##How to test the AI

1. Run the test_saved_model.py file to test the AI.
2. If it prints "model loaded successfully", that means the AI was able to load.

---

#How to save the AI

1. Run the run_saving_of_model.py to save the AI to a JSON file and to an H5 file. 
2. If the model saved successfully, then the program will print "Saved model successfully!".

##Helpful Links and Sources:

What is a neural network?: https://skymind.ai/wiki/neural-network
A Unified Embedding for Face Recognition and Clustering: https://arxiv.org/abs/1503.03832

Source Project GitHub Page: https://github.com/sumantrajoshi/Face-recognition-using-deep-learning

Keras Documentation: https://keras.io/
Tensorflow Documentation: https://www.tensorflow.org/

Python Website: https://www.python.org/
