import os
import cv2
import shutil

# constants that are used by the program to map image file locations
_move_one_directory_up = ".."
_resource_files = "resource_files"
_image_files = "images"
_haar_cascade = "haarcascade_frontalface_default.xml"
_model_directory = "Model"
_model_weights = "model.h5"
_model_json = "model.json"


# Gets the name and path of a file given a certain name to search for in that file
def get_file(searchname, path):
    for root, dirs, files in os.walk(path):
        if searchname in files:
            return os.path.join(root, searchname)


# return all files in a directory
def get_all_files(path):
    files = []
    for (path, dirs, files) in os.walk(path):
        files.extend(files)

    return files


# returns a directory name given a search parameter
def get_directory(searchname, path):
    for root, dirs, files in os.walk(path):
        if searchname in dirs:
            return os.path.join(root, searchname)


# see if a directory exists in a given path
def see_if_directory_exists(directoryname, path):
    for root, dirs, files in os.walk(path):
        if directoryname in dirs:
            return True
        else:
            return False


# saves an extracted image to a file location
def save_image_to_file(path, image):
    try:
        cv2.imwrite(path, image)
    except Exception:
        print("could not save image {}".format(path))


# sees if a file exists in a given directory to search through
def see_if_file_exists(path, file_name):
    for root, dirs, files in os.walk(path):
        if file_name in files:
            return True
        else:
            return False


# Joins a given path and a file name (also used for joining two paths)
def join_two_path_elements(path, path2):
    return os.path.join(path, path2)


# joins two paths and a file name or three paths depending on its use
def join_three_path_elements(path1, path2, path3):
    return os.path.join(path1, os.path.join(path2, path3))


# creates a directory in a path given through the path parameter
def create_directory(path):
    try:
        os.makedirs(path)
    except OSError:
        print("could not make directory for {}".format(path))


# clears a given directory
def clear_directory(path):
    try:
        for root, dirs, files in os.walk(path):
            for f in files:
                os.unlink(os.path.join(root, f))
            for d in dirs:
                shutil.rmtree(os.path.join(root, d))
    except OSError:
        print("An error occured while files in the directory {}.".format(path))


# checks to see if a given directory is empty
def see_if_directory_is_empty(path):
    if len(os.listdir(path)) == 0:
        return False
    else:
        return True

